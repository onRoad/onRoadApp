export class Token {
  public idToken: string;

  constructor(idToken?: string) {
    this.idToken = idToken;
  }
}
