import { User } from "./user.model";

export class Road {

    public id: string;
    public name: string;
    public description: string;
    public status: string;
    public owner: User;

    constructor(id?: string, name?: string, description?: string, status?: string, owner?: User) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.owner = owner;
    }
}