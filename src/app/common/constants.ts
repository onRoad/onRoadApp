export class Constants {
    /**
     * Urls
     */
    public static API_URL = 'http://localhost:8090/api';
    public static AUTHENTICATE_URL = Constants.API_URL + '/authenticate';
    public static USER_URL = Constants.API_URL + '/user/';
    public static LOGGED_USER_URL = Constants.API_URL + '/user/me';

    public static AUTHENTICATION_HEADER = 'Authorization';
}