import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { User } from "../models/user.model";
import { HttpClient } from "@angular/common/http";
import { Constants } from "../common/constants";

@Injectable()
export class UserService {
  constructor(private _httpClient: HttpClient) {}

  public get(id: number): Observable<User> {
    return this._httpClient.get<User>(Constants.USER_URL + id);
  }
}
