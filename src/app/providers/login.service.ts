import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Constants } from "../common/constants";
import { Token } from "../models/token.model";

@Injectable()
export class LoginService {
  constructor(private _httpClient: HttpClient) {}

  public doLogin(username: string, password: string): Observable<Token> {
    let params = new HttpParams()
      .set("username", username)
      .set("password", password);
    return this._httpClient.post<Token>(Constants.AUTHENTICATE_URL, params);
  }
}
