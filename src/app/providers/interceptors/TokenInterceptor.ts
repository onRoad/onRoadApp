import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { AuthService } from "../auth.service";
import { Token } from "../../models/token.model";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private _authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return Observable.fromPromise(this._authService.getAuthToken()).flatMap(
      (token: Token) => {
        if (token) {
          const headers = request.headers.set(
            "Authorization",
            "Bearer " + token.idToken
          );
          return next.handle(
            request.clone({
              headers
            })
          );
        } else {
          return next.handle(request);
        }
      }
    );
  }
}
