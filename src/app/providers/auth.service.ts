import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { Token } from "../models/token.model";

@Injectable()
export class AuthService {
  constructor(private _storage: Storage) {}

  public setAuthToken(token: Token): Promise<Token> {
    return this._storage.set("auth_token", token);
  }

  public getAuthToken(): Promise<Token> {
    return this._storage.get("auth_token");
  }
}
