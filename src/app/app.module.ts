import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { MyApp } from "./app.component";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { IonicStorageModule } from "@ionic/storage";
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS
} from "@angular/common/http";

import { TabsPage } from "./pages/tabs-group/tabs.page";
import { LoginPage } from "./pages/login/login.page";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { AuthService } from "./providers/auth.service";
import { TabsModule } from "./pages/tabs-group/tabs.page.module";
import { LoginModule } from "./pages/login/login.page.module";
import { ComponentsModule } from "./components/components.module";
import { SettingsPage } from "./pages/settings/settings.page";
import { SettingsModule } from "./pages/settings/settings.page.module";
import { UserService } from "./providers/user.service";
import { TokenInterceptor } from "./providers/interceptors/TokenInterceptor";
import { LoginService } from "./providers/login.service";
import { JwtHelper } from "angular2-jwt";

export function HttpLoaderFactory(_httpClient: HttpClient) {
  return new TranslateHttpLoader(_httpClient, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: ""
    }),
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    TabsModule,
    LoginModule,
    SettingsModule,
    ComponentsModule
  ],
  exports: [ComponentsModule],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, TabsPage, LoginPage, SettingsPage],
  providers: [
    StatusBar,
    SplashScreen,
    {
      provide: ErrorHandler,
      useClass: IonicErrorHandler
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    LoginService,
    AuthService,
    UserService,
    JwtHelper
  ]
})
export class AppModule {}
