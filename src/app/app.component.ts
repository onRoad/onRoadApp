import { Component, OnInit } from "@angular/core";
import { Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { TabsPage } from "./pages/tabs-group/tabs.page";
import { TranslateService } from "@ngx-translate/core";
import { AuthService } from "./providers/auth.service";
import { LoginPage } from "./pages/login/login.page";
import { JwtHelper } from "angular2-jwt";
import { Observable } from "rxjs/Observable";
import { Token } from "./models/token.model";

@Component({
  templateUrl: "app.component.html"
})
export class MyApp implements OnInit {
  rootPage: any = TabsPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private _translate: TranslateService,
    private _authService: AuthService,
    private _jwtHelper: JwtHelper
  ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
    _translate.addLangs([]);
  }

  ngOnInit() {
    this._translate.addLangs(["en", "es"]);
    this._translate.setDefaultLang("en");
    let browserLang = this._translate.getBrowserLang();
    this._translate.use(browserLang.match(/es|\en/) ? browserLang : "es");

    this._authService.getAuthToken().then((token: Token) => {
      if (!token || this._jwtHelper.isTokenExpired(token.idToken)) {
        this.rootPage = LoginPage;
      } else {
        this.rootPage = TabsPage;
      }
    });
  }
}
