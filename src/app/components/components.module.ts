import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { NavbarComponent } from './navbar/navbar.component';
import { RoadComponent } from './road/road.component';

@NgModule({
	imports: [IonicModule],
	declarations: [NavbarComponent,
		RoadComponent],
	exports: [IonicModule, NavbarComponent,
		RoadComponent]
})
export class ComponentsModule { }
