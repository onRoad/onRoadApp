import { Component } from '@angular/core';
import { Road } from '../../models/road.model';
import { User } from '../../models/user.model';

@Component({
  selector: 'road',
  templateUrl: 'road.component.html'
})
export class RoadComponent {

  public road: Road;

  constructor() {
    let owner: User = new User();
    owner.username = 'fakeUser';

    this.road = new Road();
    this.road.name = 'Super Road';
    this.road.description = 'Its a fake Road dude';
    this.road.status = 'ACTIVE';
    this.road.owner = owner;
  }

}
