import { Component, Input } from '@angular/core';
import { SettingsPage } from '../../pages/settings/settings.page';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'navbar',
  templateUrl: 'navbar.component.html'
})
export class NavbarComponent {

  @Input()
  public text: string;

  constructor(public navCtrl: NavController) {
    this.text = 'hello';
  }

  openSettingsMenu() {
    this.navCtrl.push(SettingsPage);
  }
}
