import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { UserService } from "../../providers/user.service";
import { User } from "../../models/user.model";

@IonicPage()
@Component({
  selector: "page-settings",
  templateUrl: "settings.page.html"
})
export class SettingsPage {
  public user: User;

  constructor(
    public navCtrl: NavController,
    private _userService: UserService
  ) {}

  ionViewDidLoad() {
    this._userService.get(1).subscribe(user => (this.user = user));
  }
}
