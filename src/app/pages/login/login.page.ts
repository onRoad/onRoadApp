import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { AuthService } from "../../providers/auth.service";
import { TabsPage } from "../tabs-group/tabs.page";
import { LoginService } from "../../providers/login.service";
import { Token } from "../../models/token.model";

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.page.html"
})
export class LoginPage {
  public username: string;
  public password: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _authService: AuthService,
    private _loginService: LoginService,
    private _toastController: ToastController
  ) {}

  ionViewDidLoad() {}

  public doLogin(): void {
    this._loginService.doLogin(this.username, this.password).subscribe(
      (token: Token) => {
        this._authService.setAuthToken(token).then((token: Token) => {
          this.navCtrl.push(TabsPage);
        });
      },
      error => {
        this._toastController
          .create({
            message: "Error in authentication", //this.translateService.instant('LOGIN.WrongCredentials'),
            duration: 3000
          })
          .present();
      }
    );
  }
}
