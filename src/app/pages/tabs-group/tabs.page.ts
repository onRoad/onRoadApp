import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { HomePage } from './home/home.page';
import { TrendingPage } from './trending/trending.page';
import { ChatPage } from './chat/chat.page';

@IonicPage()
@Component({
  templateUrl: 'tabs.page.html'
})
export class TabsPage {

  homeRoot = HomePage;
  trendingRoot = TrendingPage;
  chatRoot = ChatPage;

  constructor() {
  }

}
