import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-trending',
  templateUrl: 'trending.page.html',
})
export class TrendingPage {

  constructor(public navCtrl: NavController) {
  }

  findPeople() {
    console.log('finding...');
  }

}
