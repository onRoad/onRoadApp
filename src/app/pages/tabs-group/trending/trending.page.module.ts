import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrendingPage } from './trending.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [TrendingPage,],
  imports: [IonicPageModule.forChild(TrendingPage), ComponentsModule],
  exports: [TrendingPage, ComponentsModule]
})

export class TrendingModule { }
