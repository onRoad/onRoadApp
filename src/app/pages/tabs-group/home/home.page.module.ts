import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [HomePage],
  imports: [IonicPageModule.forChild(HomePage), ComponentsModule],
  exports: [HomePage, ComponentsModule]
})

export class HomeModule { }
