import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsPage } from './tabs.page';
import { HomeModule } from './home/home.page.module';
import { TrendingModule } from './trending/trending.page.module';
import { ChatModule } from './chat/chat.page.module';

@NgModule({
    declarations: [TabsPage],
    imports: [IonicPageModule.forChild(TabsPage), HomeModule, TrendingModule, ChatModule],
    exports: [HomeModule, TrendingModule, ChatModule, TabsPage]
})
export class TabsModule { }